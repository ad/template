# Animations

<video controls>
  <source data-src="assets/cluster.webm" type="video/webm" />
  <source data-src="assets/cluster.mp4" type="video/mp4" />
</video>

--

{% background #232323 %}

## autoplaying

<video autoplay loop>
  <source data-src="assets/flickr.webm" type="video/webm" />
  <source data-src="assets/flickr.mp4" type="video/mp4" />
</video>
